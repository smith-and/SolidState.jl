# SolidState Documentation

This documentation explains the usage and underlying algorithms invovled in the SolidState package. You know?

```@meta
CurrentModule = SolidState
```

## Contents

```@contents
```

## API

```@autodocs
Modules = [SolidState]
```

## Index

```@index
```
